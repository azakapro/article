package main

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"fmt"
	"strconv"

	_ "bootcamp/article/server/docs"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

var inMemory storage.ArticleStorage

// @title Swagger Example API
// @version 1.1
// @description This is a sample article demo.
// @termsOfService https://udevs.io
// @host localhost:7070
func main() {

	gin.SetMode(gin.DebugMode)
	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())

	inMemory = make(storage.ArticleStorage)

	r.POST("/articles", CreateHandler)
	r.GET("/articles", GetAllHandler)
	r.GET("/articles/:id", GetByIDHandler)
	// r.PUT("/articles/:id", UpdateHandler)
	// r.DELETE("/articles/:id", DeleteHandler)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.Run(":7070")
}

type DefaultError struct {
	Message string `json:"message"`
}

type ErrorResponse struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}
type SuccessResponse struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// CreateHandler godoc
// @Router /articles [POST]
// @Summary Create an article
// @Description it creates an article based on input data
// @ID create-handeler
// @Accept  json
// @Produce  json
// @Param data body models.Article true "Article data"
// @Success 200  {object} SuccessResponse
// @Failure 400,404 {object} ErrorResponse
// @Failure default {object} DefaultError
func CreateHandler(c *gin.Context) {
	var a1 models.Article
	err := c.BindJSON(&a1)
	if err != nil {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	err = inMemory.Add(a1)
	if err != nil {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	c.JSON(200, SuccessResponse{
		Message: "Ok",
	})
}

// GetAllHandler godoc
// @ID get-all-handler
// @Summary List articles
// @Description get all articles
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Article
// @Failure default {object} DefaultError
// @Router /articles [get]
func GetAllHandler(c *gin.Context) {
	resp := inMemory.GetAll()

	c.JSON(200, resp)
}

func GetByIDHandler(c *gin.Context) {
	idStr := c.Param("id")
	idNum, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	fmt.Println(idNum)
	resp, err := inMemory.GetByID(int(idNum))
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"res": resp,
	})
}
