package storage

import (
	"bootcamp/article/models"
	"errors"
)

var ErrorNotFound = errors.New("not found")
var ErrorAlreadyExists = errors.New("already exists")

type ArticleStorage map[int]models.Article

func (storage ArticleStorage) Add(entity models.Article) error {
	if _, ok := storage[entity.ID]; ok {
		return ErrorAlreadyExists
	}
	storage[entity.ID] = entity
	return nil
}

func (storage ArticleStorage) GetByID(ID int) (models.Article, error) {
	var resp models.Article
	resp = storage[ID]
	if val, ok := storage[ID]; ok {
		resp := val
		return resp, nil
	}
	return resp, ErrorNotFound
}

func (storage ArticleStorage) GetAll() []models.Article {
	var resp []models.Article
	for _, v := range storage {
		resp = append(resp, v)
	}
	return resp
}

func (storage ArticleStorage) Search(str string) []models.Article {
	var resp []models.Article
	//
	// TODO
	//
	return resp
}

func (storage ArticleStorage) Update(entity models.Article) error {
	//
	// TODO
	//
	return nil
}

func (storage ArticleStorage) Delete(ID int) error {
	//
	// TODO
	//
	return nil
}
