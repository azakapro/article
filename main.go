package main

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"fmt"
	"time"
)

var inMemory storage.ArticleStorage

func main() {

	inMemory = make(storage.ArticleStorage)
	var a1 models.Article
	a1.ID = 1
	a1.Title = "Lorem"
	a1.Body = "Lorem ipsum"
	var p models.Person = models.Person{
		Firstname: "John",
		Lastname:  "Doe",
	}
	a1.Author = p
	t := time.Now()
	a1.CreatedAt = &t
	err := inMemory.Add(a1)
	if err != nil {
		fmt.Println(err)
	}
	err = inMemory.Add(a1)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(inMemory)
}
